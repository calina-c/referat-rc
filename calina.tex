\chapter{Remote electronic voting}
Remote e-voting is different from its supervised sibling by a set of issues 
regarding vote verifiability, from both the voter's and the democratic system's 
points of view. Proper remote 
e-voting systems could improve the participation of the younger demographic in 
the election process, as well as improving accessibility and preventing some 
types of fraud, which is why the 
preoccupation exists for this subject.

The following chapter presents an overview of remote electronic voting issues, 
such as its particularities compared to other voting types, verifiability and 
legal issues from a 
technological perspective, a few case studies and pilot programs running 
different remote technologies, as well as a final architecture proposition for a 
remote voting system.
\section{Difference from other voting types}
\subsection{Compared to traditional voting types}
The process of remote electronic voting differs from traditional voting by many 
aspects. One of these is the absence of government monitoring, which may pose 
problems with vote fraud, such 
as coercion, family voting and many others. In essence, the legal principle of 
the free and secret vote can not be ensured remotely.

R.Krimmer et al \cite{krimmer2005bits} introduce a set of comparison criteria 
between remote e-voting and its closest traditional counterpart, which is postal 
voting. Already implemented in 
some countries, postal voting bears many similarities with remote e-voting, 
including the accessibility advantage, the absence of a need to travel to a 
polling station and the fraud 
suspicions. They take into consideration the possibility of vote fraud by either 
malicious software or voter impersonation, but find that remote e-voting offers 
improved traceability and 
speed. Traceability was ensured by the possibility for voters to tag their cast 
vote with a pseudonym, allowing them to check whether it was included in the 
final ballot without in any way 
linking the person with the vote itself, thus providing secrecy. This feature is 
not available for postal voting, where someone can open a person's envelope or 
prevent it from reaching the 
polling station without the user ever being aware of this.

\subsection{Compared to supervised e-voting}
When comparing supervised and unsupervised e-voting the problem is somehow 
similar to treating public or privately owned networks. The accessibility 
advantage, much praised for electronic 
voting of both kinds, differs in regards of the owner of the specialised 
equipment. While in supervised electronic polling stations the user may have at 
their disposal a vast number of 
disability-related input devices, from headphones, pedals, joysticks, sip-n-puff 
devices etc., in case of remote e-voting the user must own their device of 
choice privately. It also can not 
be verified that the person votes independently or asks for third party help.

\section{Sociological considerations and verifiability}
A remote voting system poses many problems from a legal perspective. Being a 
core element of democratic societies, voting through an electronic gateway is 
prone to mistrust. 
A. Prosser et al \cite{prosser2004dimensions} present a graphical explanation of 
the pressure each pillar: technology, law, politics and society.

From the technological point of view, a remote voting system must be secure, 
fast, scalable and flexible enough to accomodate a diversity of input devices. 
The system should also provide 
tracing and encoding via cryptographic verifiability (see \ref{cvr}) in order to 
sustain the law-derived principles of freedom and secrecy. Politicians and law 
experts take great interest 
in ensuring that no fraud may take place, not even under malicious circumstances 
e.g. a foreign government may hack the system and interfere with the election 
process. The society is formed 
of voters with interest in legitimate results, and they need to trust the 
computations completely. This issue has been much discussed, in relation to the 
open source principle and computer 
trust issues \cite{Thompson:1984:RTT:358198.358210}.

\subsection{Cryptographic verifiability} \label{cvr}
The subject of cryptographic verifiability is universal to electronic voting, 
regardless of method, so this subsection will serve only as a reminder of the 
role and issues of cryptography, 
as well as its applicability for remote e-voting in particular. Cryptographic 
methods are used in order to ensure 3 basic principles of voting

\begin{itemize}
\item individuality: a user may check whether or not their vote was included in 
the final ballot
\item universality: a user may check that the outcome of the vote is based 
exclusively on the votes cast
\item eligibility: every vote was legitimate, from a person with the right to 
vote
\end{itemize}

The individuality principle conflicts with the secrecy principle, unless special 
tools are put in place to decouple the user from the explicit vote, while 
maintaining the user's certainty 
using a receipt. The Mercuri method requires that printed receipts be offered to 
voters. Combining this with punchscan ballots or Pret-a-voter methods encodes 
the vot receipt so that only 
the user knows what they voted, freeing them from vote fraud and coercion 
suspicions. Offering printed receipts impossible, though, with mobile terminal 
users and PC users without printers 
installed. A digital receipt could be made available, offering some of the 
advantages of the classical Mercuri method.

Other methods to ensure vote integrity are autiding the terminal used before any 
vote is cast (although usually it is difficult to detect malicious software, 
since often it erases 
the traces of its actions), as well as auditing and tracing the vote throughout 
the network, ensuring no malicious proxies were encountered and vote integrity 
was not lost. This issue 
mimics the same problem with DRE devices and even with physical paper ballots, 
which must always be supervised to prevent tampering from different parties.

\section{Security considerations}
\subsection{Trojans}
R.Krimmer et al \cite{krimmer2005bits} do cite Trojans as a possible security 
risk against the free and secret principle: a Trojan may change a voter's choice 
before casting (thus breaking 
the free principle) or it may divulge the data to third parties (breaking vote 
secrecy). Avi Rubin et al. \cite{rubin2001security} offer even more insight. 
They list a series of problems 
which are caused by Trojans, as well ast the difficulty to detect the program, 
since after making the malicious changes, it erases itself. Even more dangerous, 
they argue, is automated 
installation available through company servers or local networks with 
appropriate settings. A possible solution to this would be enforcing a trusted 
application source when installing 
applications, such as Apple's iOS does on its devices. This prevents users from 
installing malicious software. The AppStore's engineers are also required to 
carefully inspect every software 
before it becomes publicly available.

\subsection{Proxies and Social Engineering}
Rubin also brings up the problem of privately owned malicious proxies, which can 
impersonate the voting system and ``trick'' the user into thinking that they are 
voting. Well known scams 
rely on people's lack of attention to detail, providing them with pages very 
similar or even identical to an official web page, then collecting users' data. 
In fact, the vote may never 
reach the official servers or, even worse, be hijacked and falsified, then sent 
further. Both physical coercion and silent coercion (like in this case) may be 
detected with specialised 
systems (see \ref{cc}).

\subsection{DDOS Attacks}
DDOS (Distributed Denial of Service) attacks aim to make networks and machines 
unusable for their intended users. Needless to say redundant servers with load 
balancing and database sharding 
are a must of any remote e-voting system, especially when dealing with public 
elections where the threat is bigger.

\section{Case studies and pilot programs}
\subsection{GI private election}
The solution presented by the previously mentioned paper of R.Krimmer et al 
\cite{krimmer2005bits} related to vote by postal services was a test program run 
by the informatics company GI 
for board chairman elections. They present their solution as a prototype one, 
noting that indeed in case of public elections the stakes would be higher and 
security should be better 
implemented.

They present the token solution of vote tracking as previously explained, along 
with redundant servers and load balancing techniques used to prevent DDoS 
Attacks.Their approach 
also mentions a separation between the two servers: one which tracks votes 
themselves and another that registers if a user has voted yet. This 
differentiation is done in order to remove 
traces of votes to outsiders, ensuring vote secrecy. Another important aspect of 
the software is that on the main servers no other software should be allowed to 
run and access to the server 
rooms should always be done with close monitoring and never by one single person 
at a time.

\subsection{Estonia}
Estonia implemented its remote e-voting system with success (there is a 
continuous growth of percentage of people who vote online among the Estonian 
people) as part of an e-minded 
initiative by the government in 2001. The Estonian electronic voting system is 
exclusively remote; there are no electronic polling stations. Security is 
modelled after advance voting and 
postal voting, with legitimity of ballots being checked using peoples' very 
identities. The Estonian smart ID card contains a chip which can be read using a 
smart card reader, allowing the 
owner of such a cart to provide identification and access online vote casting 
mechanisms.

Another interesting particularity about this system is that it also allows 
voters to change their minds, up until one day before elections. During election 
days, a person may vote at a 
polling station, which invalidates their online vote. Since the first elections, 
the percentage of Internet voters has increased up to 24.3\% (wikipedia 
\cite{wiki:votestonia}). Regarding 
the trust issue, it should be noted that Estonian vote counting software and 
documentation is posted online, open source and available for inspection 
(\cite{vvkee}).

\subsection{Mobile based voting}
A suite of mobile-based voting systems have also been proposed. One advantage is 
the security features already implemented in mobile phones (especially 
smartphones), such as wireless certificate authorisation. One such paper 
\cite{kim2007electronic} proposes that instead of issuing codes to users, their 
phones may be directly pre-authorised. Their proposed system also includes an 
asymmetrical key encryption mechanism to enable vote secrecy.

In their 2007 paper \cite{alefragis2007mobile} Aefragis et. al propose a system based on ballot duplication and moderation by election stakeholders. Their system has a front-end which was not unusal for that time, i.e. J2ME bytecode, although in more modern times native applications or web interfaces are preferred for UIs. They also include a SMS gateway to ensure that even phones that are incompatible with modern standards have access to the voting platform.

In this case, security is ensured with HTTPS ans SSL, but the interesting particularity is the introduction of multiple identical ballot boxes, i.e. the vote is cloned into several ballots, where members from different parties, government and NGO observers may supervise the voting process. Any discrepancy taking place inside one ballot or inbetween ballots is then easily detected. This model of consistent multiple boxes is, of course, unfit for direct voting and can only be applied in e-voting.

\subsection{Caveat Coercitor} \label{cc}
The Caveat Coercitor model \cite{grewal2013caveat} is much different from the previously defined solutions. The idea behind it is that coercion can never be fully prevented, so it is a better idea to tolerate coercion, but make it evident. However, this poses some serious issues in the voting process, such as devising a test to identify coercion.

The simplest idea is of course asking the voter whether he was coerced into placing their vote. This solution works well for direct coercion, but does not detect the subtleties of silent coercion e.g. trojan attacks, which act as coercion, but directly onto the vote and not onto the voter. The generally acknowledged solution is voting a number of times, with discrepancy detection i.e. if someone makes the same choice every time, then the vote seems clean, else it could be coerced.

Problems arising from such a system are counter-acting the ``easy'' factor of electronic voting, by asking the user to vote several times. Furthermore, coercion simulation is another serious problem since minority parties or special interest groups could fake coercion in order to cast doubts over the vote results and force re-elections.

\section{Proposed remote voting system}
The previous section presented various issues and implementations of remote electronic voting systems. Based on all these solutions, the following section proposes a model for remote e-voting which combines the advantages into a single hypothetical system. Let this be known as the BEV (Better Electronic Voting) solution. BEV will reference, on occasion, particularities of the Romanian society and aims to solve some of the Romanian election issues.

\subsection{Registration and authentication}
July 2015 is set as the date Romania will initiate the process of changing the current identity cards with smart identity cards, containing a chip. However, the smart ID cards are not the only option, as some people may still choose a regular ID card. Since this refusal of smart ID cards is due to technology rejection, it is my opinion that people who do not desire smart ID cards will most likely not participate in any electronic voting process. This means that registration to vote is a non-issue for smart card owners and, following the Estonian model, it is likely that smart card readers will be available for usage as identity verification devices.

A broader solution would be the idea of having voters register in the way most convenient to them, i.e. either by requesting a voting token to be input to BEV, or by pre-authenticating their devices. The latter option is safer since it is fairly noticeable if someone has direct access to a device and poses as another voter, while detecting family voting with tokens assigned to individual people is essentially impossible without supervision. Modern mobile devices and some PCs have options in place that prevent outside meddling via a software route.

An essential part of the voting process is that, just like the Estonian model proposed, a larger period of time be alotted to people to cast change their votes. Furthermore, on the final day of voting, any person should be able to vote directly, at polling stations and have this final decision considered. Not only does this process support coercion detection via the Caveat Coercitor, but it also improves the chances that, in case the voter suspects malicious software on their computer, they can check their own vote and even superseed it with a physical one.

\subsection{Malware prevention}
For effective malware prevention, devices with strict installation rules are required. For example iOS devices closely resemble the secure installation model, as by default they do not allow the user to install software from other sources than the official Apple Store, where engineers check applications for malware on a regular basis and forbid malicious software to reside inside their repositories. However, this option too can be unchecked on the Mac devices if the user wants to download application archives off the Internet. This means that full protection is only available for mobile devices such as the iPhone, iPod and iPad. This type of feature is added also on Linux machines and Android mobile devices, but can also be disabled, while Windows remains a security risk due to poor user rights management and various daemons that enable silent remote connection.

In my opinion, only a system that uses trust-based installation should be eligible for native applications, while simple web-based interfaces, SMS gateways and other mechanisms for circumventing the official BEV authorisation policy should not be allowed.

\subsection{Man-in-the middle prevention}
A standard security measure known in literature is employing a HTTPS medium, which provides encryption of the messages to prevent any foreign proxies from intercepting, reverse engineering or replacing original messages with fake ones. The danger is real, as illustrated by the two main use cases:
\begin{itemize}
\item false confirmation: the user is prompted with a confirmation message when in fact their vote was not recorded
\item vote falsifying: the user is prompted with a confirmation message when in fact their vote was changed and relayed to the real servers as per the interceptor's desire
\end{itemize}

HTTPS is not a protocol by itself, but rather a combination of the HTTP and SSL/TLS protocol, which is the real provider of the encryption mechanism. HTTPS is available on modern browsers and BEV could be configured to require HTTPS requests only.

\subsection{Fraud detection and prevention}
\subsubsection{Coercion detection}
In my opinion a coercion detection system that requires voting multiple times is not feasible, since the issues we are working to solve are precisely user accessibility and easiness to vote. Any system that requires voting multiple times or performs unusual tests on the user may impair these exact improvements.
\subsubsection{Identical ballots}
However, fraud detection in direct server access could be more easily detected, provided multiple identical ballots exist and the system has a desired final stable state which can be used as a baseline. Servers in a distributed network duplicate vote content independently of user data and duplicate recieved nodes onto other servers.

Considering this set of initial assumptions, it is required that the system reaches a stable state at the end of voting, where ballots are identical. Any inconsistency between nodes can not only indicate fraud, but also correctly identify by a differential analysis the corrupted votes. Server access logs can also provide clues as to the attacker's location and/or identity. What is important to note, however, is the weakness of eventual stability, i.e. for a period of time until full replication takes place, the system will always appear to be inconsistent.

The ballots are ideally monitored as in the \cite{alefragis2007mobile} paper, by representatives of each party involved in the elections, as well as NGOs and independent political monitors.

\subsection{Conclusions - Applied cryptographic verifiability}
As previously presented, the 3 main issues we are concerned with in remote electronic voting are related to cryptographic verifiability. Below the 3 are again listed, along with their solution in the context of our proposed system:
\begin{itemize}
\item identifiability: encrypting the vote with a private key and offer an encrypted receipt (Mercuri method) through which the user can identify their vote in any of the duplicated ballots
\item universality: multiple identical ballots offer insight into possible vote fraud, while having an open-source program ensures that users may audit the voting code and contribute to tests
\item eligibility: not an issue with smart cards (tampering with a personal document is more evident than tampering with various token forms, since people tend to have their ID cards upon themselves at all times)
\end{itemize}

It is much likely, however, that the implementation using smart cards requires a much to optimistical release schedule for the new documents. Detecting family voting is virtually impossible with snail-mail distributed tokens.

It is my opinion that two large factors influence the applicability of such a system: smart ID cards being available to the majority of the voters and the existence of an application store that doesn't allow safety overrides, to serve as an installation source.
